// @ts-check
const sass = require("node-sass");
const crypto = require("crypto");
const path = require("path");
const fs = require("fs");

const currentDate = new Date().valueOf().toString();
const random = Math.random().toString();
const hash = crypto
  .createHash("sha1")
  .update(currentDate + random)
  .digest("hex");

/**
 * @param {String} filePath
 * @returns {Boolean}
 */
function hasDirectory(filePath) {
  let hasDir = false;
  try {
    const absolutePath = path.resolve(filePath);
    const pathObject = path.parse(absolutePath);
    fs.accessSync(pathObject.dir);
    hasDir = true;
  } catch (e) {
    hasDir = false;
  }
  return hasDir;
}

/**
 * @param {String} filePath
 */
function makeDir(filePath) {
  try {
    const absolutePath = path.resolve(filePath);
    const pathObject = path.parse(absolutePath);
    fs.mkdirSync(pathObject.dir);
  } catch (error) {
    throw error;
  }
}

function build(options) {
  if (options.dest.includes(".css")) {
    options.dest = options.dest.replace(".css", "");
  }
  const ops = {
    file: options.src,
    outFile: `${options.dest}-${hash}.css`,
    outputStyle: options.outputStyle
  };

  function callBack(error, result) {
    if (error) {
      throw error;
    }
    if (!hasDirectory(ops.outFile)) {
      makeDir(ops.outFile);
    }
    fs.writeFile(ops.outFile, result.css, function(err) {
      if (err) {
        throw err;
      } else {
        console.log(`wrote ${ops.outFile}`);
        return;
      }
    });
  }
  sass.render(ops, callBack);
}

/**
 * @param {string} cssPath
 * @returns {Promise<string>}
 */
function findCssFile(cssPath) {
  return new Promise((resolve, reject) => {
    fs.readdir(cssPath, (error, files) => {
      if (error) {
        reject(error);
      } else {
        files.forEach(file => {
          if (file.endsWith("css")) {
            resolve(file);
          }
        });
      }
    });
  })
}

module.exports.build = build;
module.exports.sass = sass;
module.exports.findCssFile = findCssFile;
