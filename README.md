# sass-hash

[![Greenkeeper badge](https://badges.greenkeeper.io/danmademe/sass-hash.svg)](https://greenkeeper.io/)

Hashes your SASS for maximum Cache

### Requires node-sass!

```sh
npm i node-sass sass-hash
```


```js
const sassHash = require("sass-hash"); 
const cssPath = path.join(__dirname, "./assets/rendered/");
sassHash.findCssFile(cssPath)
    .then(cssFile => {
       const cssPath = `/assets/rendered/${cssFile}`;
    })
    .catch(error => {throw error; });
```

## or .. via CLI

```sh
sass-hash --output-style compressed app/assets/scss/app.scss app/assets/rendered/style
```

```
sass-hash [options] <input> <output>
```

### Options


right now im only supporting output-style.. i'll get the rest of node-sass's stuff working
